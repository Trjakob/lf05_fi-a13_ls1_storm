package uebungen1;

import java.util.Scanner;

public class Ohmschisgesetz {

	public static void main(String[] args) {
		
		//Variablen definieren
		float r;
		float u;
		float i;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben Sie an welche Groesse berechnet werden soll (R/U/I) : ");
		String groesse_zuBerechnen_String = scanner.next();
		char groesse_zuBerechnen = groesse_zuBerechnen_String.charAt(0);
		
		switch(groesse_zuBerechnen) {
		//Widerstand in Ohm berechnen
		case('R'):
			System.out.println("Geben Sie den Wert für die Spannung U ein: ");
			u = scanner.nextFloat();
			
			System.out.println("Geben Sie den Wert für die Stromstärke I ein: ");
			i = scanner.nextFloat();
			
			System.out.printf("R = U/I = %.2f/%.2f = %.2f",u,i,(u/i));
			break;
		
		//Spannung in Volt berechnen
		case('U'):
			System.out.println("Geben Sie den Wert für den Widerstand R in Ohm ein: ");
			r = scanner.nextFloat();
			
			System.out.println("Geben Sie den Wert für die Stromstärke I in Ampere ein: ");
			i = scanner.nextFloat();
			
			System.out.printf("U = R*I = %.2f*%.2f = %.2f",r,i,(r*i));
			break;
			
		//Stromstärke in Ampere berechnen
		case('I'):
			System.out.println("Geben Sie den Wert für den Widerstand R in Ohm ein: ");
			r = scanner.nextFloat();
			
			System.out.println("Geben Sie den Wert für die Spannung U in Volt ein: ");
			u = scanner.nextFloat();
			
			System.out.printf("I = U/R = %.2f/%.2f = %.2f",u,r,(u/r));
			break;
			
		//ungültige Usereingabe
		default:
			System.out.println("Ungültige Eingabe > geben sie R / U / I mit");
		}
		

	}

}
