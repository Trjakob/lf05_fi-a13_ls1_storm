package uebungen1;

import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		//Abfrage gewicht
		System.out.println("Bitte geben Sie ihr Körpergewicht an (in kg) : ");
		float gewicht = scanner.nextFloat();
		
		//Abfrage groesse
		System.out.println("Bitte geben Sie ihre Körpergroesse an (in m) : ");
		float groesse = scanner.nextFloat();
		
		//Abfrage Geschlecht
		System.out.println("Bitte geben Sie ihr Geschlecht an (m/w/d): ");
		String geschlecht_String = scanner.next();
		char geschlecht = geschlecht_String.charAt(0); //Weiterrechnung mit char
		
		//finale Ausgabe der BMI Klassifikation
		System.out.println("Ihre BMI Klassifikation: "+ bmi_classification(bmi_calculation(gewicht, groesse), geschlecht));
		
		scanner.close();
		

	}
	
	public static String bmi_classification(float givenBmi, char geschlecht) {
		String s;
		if(geschlecht == 'w') {
			if(givenBmi < 19) {
				s = "untergewicht";
			} else if(givenBmi < 24) {
				s = "normalgewicht";
			} else {
				s = "uebergewicht";
			}
		} else if(geschlecht == 'm') {
			if(givenBmi<20) {
				s = "untergewicht";
			} else if(givenBmi<25) {
				s = "normalgewicht";
			} else {
				s = "uebergewicht";
			}
		} else {
			s = "BMI für "+geschlecht+" nicht definiert";
		}
		return s;
	}
	
	public static float bmi_calculation(float gewicht, float groesse) {
		return gewicht/(groesse*groesse);
	}

}
