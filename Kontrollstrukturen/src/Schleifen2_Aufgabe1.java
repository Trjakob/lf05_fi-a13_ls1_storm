import java.util.Scanner;
public class Schleifen2_Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner eingabe = new Scanner(System.in);
		int n =eingabe.nextInt();
		//aufzählend 
		int zahl = 1;
		while(zahl<n){
			System.out.println(zahl);
			zahl++;
		}
		
		//jetzt runterwerts
		zahl = n;
		
		while(zahl >0) {
			System.out.println(zahl);
			zahl--;
		}

	}

}
