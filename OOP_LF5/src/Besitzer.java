import java.util.ArrayList;
import java.util.Arrays;

public class Besitzer {
	
	private String name;
	private String vorname;
	
	ArrayList<Konto> myAccounts;

//	Constructor
	public Besitzer() {
		myAccounts = new ArrayList<Konto>();
	}
	
	public Besitzer(String name, String vorname, Konto account1, Konto account2) {
		this.name = name;
		this.vorname = vorname;
		myAccounts = new ArrayList<Konto>(Arrays.asList(account1, account2));
	}
	
//	Getter and Setter
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
//	Methods
	
	public void gesamtUebersicht() {
    	for(Konto acc : myAccounts) {
	    	System.out.printf("Iban >> %s\tacc nr. >> %d\n", acc.getIban(), acc.getKontonummer());
		}
	}
	
	public void gesamtGeld() {
		double networth = 0;
		String worthPerAccount= "";
		for(Konto acc : myAccounts) {
			worthPerAccount +="acc nr.  "+acc.getKontonummer()+"-->"+acc.getKontostand()+"€";
			networth += acc.getKontostand();
			
		}
		System.out.printf("Gesamtvermögen %.2f\t(%s)\n",networth, worthPerAccount);
	}
}
