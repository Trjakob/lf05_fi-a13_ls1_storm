package Raumschiffe;

import java.util.ArrayList;

public class Raumschiff {
	
	private int photonentorpedos;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	
	private String schiffsname;
	
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	//Constructor
	public Raumschiff() {
		broadcastKommunikator = new ArrayList<String>();
		ladungsverzeichnis = new ArrayList<Ladung>();
	}

	public Raumschiff(int potonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this();
		this.photonentorpedos = potonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	//Verwaltungsmethoden
	public int getPotonentorpedoAnzahl() {
		return photonentorpedos;
	}

	public void setPotonentorpedoAnzahl(int potonentorpedoAnzahl) {
		this.photonentorpedos = potonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	//weitere Methoden
	public void nachrichtAnAlle(String message) {
		this.broadcastKommunikator.add(message);
		System.out.println(message);
	}
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator;
	}
	
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonenTorpedosSchieﬂen(Raumschiff r) {
		if(this.photonentorpedos == 0) {
			nachrichtAnAlle("-=*click*=-");
		} else {
			this.photonentorpedos -= 1;
			nachrichtAnAlle("Photonentorpedo abgeschosssen");
			r.treffer();
		}
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		this.photonentorpedos += anzahlTorpedos;
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(this.energieversorgungInProzent<50) {
			nachrichtAnAlle("-=*click*=-");
		} else {
			this.energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			r.treffer();
		}
	}
	
	//Notiz: Abweichung vom OOD - semantische Gr¸nde - Raumschiff vermerkt jetzt seine eigenen Treffer und nicht die Treffer f¸r andere
	public void treffer() {
		System.out.printf("%s wurde getroffen!", this.schiffsname);
		this.schildeInProzent -= 50;
		if(this.schildeInProzent <= 0) {
			this.schildeInProzent = 0; //Vermeidung negativer Prozentwerte
			this.huelleInProzent -= 50;
			this.energieversorgungInProzent -= 50;
		}
		if(this.huelleInProzent <= 0) {
			this.lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle(this.schiffsname+": Lebenserhaltungssysteme zerstˆrt");
		}
	}
	
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		//TODO
	}
	
	public void zustandRaumschiff() {
		System.out.printf("Zustand %s:\n\tEnergieversorgung: %s%%\n\tSchilde: %s%%\n\tH¸lle: %s%%\n\tlebenserhaltungssysteme: %s%%\n\tAndroidenanzahl: %s\n",
				schiffsname, energieversorgungInProzent, schildeInProzent, huelleInProzent, lebenserhaltungssystemeInProzent, androidenAnzahl);
	}
	
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladungsverzeichnis "+this.getSchiffsname()+":");
		for(Ladung ladung : this.ladungsverzeichnis) {
			System.out.printf("[Bezeichnung: %s; Menge: %s]\n",ladung.getBezeichnung(),ladung.getMenge());
		}
	}
	
	public void ladungsverzeichnisAufraeumen() {
		this.ladungsverzeichnis.clear();
	}
}
