package Raumschiffe;


public class Ladung {
	
	private String bezeichnung;
	private int menge;
	
	//Constructor
	public Ladung() {
		
	}
	
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	//Verwaltungsmethoden
	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}
	//weitere Methoden
	@Override
	public String toString() {
		return super.toString()+"[ Bezeichnung: "+this.bezeichnung+"; menge: "+this.menge+" ]";
	}
	
	
	
	
	
}
