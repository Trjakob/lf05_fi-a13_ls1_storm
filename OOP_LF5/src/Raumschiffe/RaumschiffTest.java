package Raumschiffe;


public class RaumschiffTest {

	public static void main(String[] args) {
		//Test Raumschiffe
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh¥ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni¥Var");	
		
		//Test Ladungsobjekte
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat¥leth Klingonen Schwert", 200);
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma Waffe", 50);
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		
		//Verkn¸pfungen ¸ber addLadung()
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);
		
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);
		
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);
		
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.ladungsverzeichnisAusgeben();

		klingonen.zustandRaumschiff();
	}
}
