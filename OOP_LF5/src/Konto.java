
public class Konto {
	
	private String iban;
	private int kontonummer;
	private double kontostand;
	//Constructor
	public Konto() {
		
	}
	
	public Konto(String iban) {
		this.iban = iban;
	}
	
	public Konto(String iban, int kontonummer) {
		this(iban);
		this.kontonummer = kontonummer;
	}
	
	//Methods
	
	public void abheben(double abheben) {
		this.kontostand -=abheben;
	}
	
	public void einzahlen(double einzahlung) {
		this.kontostand +=  einzahlung;
	}
	
	public void ueberweisen(Konto destKonto, double betrag) {
		this.kontostand -= betrag;
		destKonto.einzahlen(betrag);
	}
	
	//Getter und Setter
	public String getIban() {
		return this.iban;
	}
	
	public void setIban(String iban) {
		this.iban = iban;
	}
	
	public int getKontonummer() {
		return this.kontonummer;
	}
	
	public void setKontonummer(int kontonummer) {
		this.kontonummer = kontonummer;
	}
	
	
	public double getKontostand() {
		return kontostand;
	}

	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}
	
	
	
}
