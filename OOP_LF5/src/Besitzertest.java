
public class Besitzertest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Konto account1= new Konto("DE38",1);
		Konto account2= new Konto("DE23");
		Besitzer myOwner= new Besitzer("Tim", "Tom", account1, account2);
		
		
		//Manipulationen
		account2.setKontonummer(2);
		
		//TestMethoden
		myOwner.gesamtGeld();
		myOwner.gesamtUebersicht();
		
		//einzahlen
		account1.einzahlen(20);
		account2.einzahlen(10);
		
		
		//Übersicht nach Einzahlung
		myOwner.gesamtGeld();
		myOwner.gesamtUebersicht();
		
		
		//Überweisung
		account1.ueberweisen(account2, 5);
		
		//Übersicht nach Einzahlung
		myOwner.gesamtGeld();
		myOwner.gesamtUebersicht();
	}

}
