package general;
import java.util.Scanner;

public class ScannerClass {
	
	ScannerClass() {
		
		Scanner scanner = new Scanner(System.in); //scanner reads from console (System.in)
		
		System.out.println("What is your name?");
		String name = scanner.nextLine();
		
		System.out.println("Hello "+name);
		System.out.println("How old are you?");
		int age = scanner.nextInt();
		scanner.nextLine(); //clear the scanner-line to prevent ignoration of next Inputs (necessary after everything the scanner reads without the nextLine() statement)
		
		System.out.println("Nice to meet you Mr."+age+" years old");
		
		scanner.close(); //always close scanner after usage
		
	}
}
