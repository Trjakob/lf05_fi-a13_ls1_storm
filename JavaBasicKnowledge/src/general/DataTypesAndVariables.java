package general;

public class DataTypesAndVariables {
	
	//variables
	boolean b = false; 				//(value: True or False)							(size: 1 bit)	(type: primitive)
			
	byte by = 21; 					//(value: -128 to 127)								(size: 1 byte)	(type: primitive)
	short sh = 1000;				//(value: -32768 to 32767)							(size: 2 bytes)	(type: primitive)
	int in = 1999;					//(value: -2billion to 2billion)					(size: 4 bytes)	(type: primitive)
	long lo = 451357885645689L;		//(value: -9quintillion to 9 quintillion)			(size: 8 bytes)	(type: primitive)
			
	float fl = 4.234235f; 			//(value: fractional numb up to 6-7 digits)			(size: 4 bytes)	(type: primitive)
	double doub = 4.2345345; 		//(value: fractional numb up to 15 digits)			(size: 8 bytes)	(type: primitive)
			
	char chr = 'f';					//(value: single character / letter / ASCII value)	(size: 2 bytes)	(type: primitive)
	String str = "Hello";			//(value: a sequence of characters)					(size: varies)	(type: reference)
	
	
	//different types:
	//primitive						//reference
	//-----------------------		//----------------
	// - 8 types(int, boolean...)	- unlimited (user defined)
	// - store data	(values)		- store an address (refer to an object)
	// - faster						- slower

	
}
