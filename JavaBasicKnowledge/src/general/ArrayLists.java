package general;

import java.util.ArrayList;

public class ArrayLists {
	
	ArrayLists() {
		
		// ArrayList = 	a resizable array
		//				- elements can be added and removed after initializing
		//				- stores reference types instead of primitive types (if you want to store primitive types you need
		//				to use the corresponding Wrapper class!)
		
		ArrayList<String> food = new ArrayList<String>();
		
		//add Values
		food.add("Pizza");
		food.add("Hamburger");
		food.add("Hotdog");
		
		//print ArrayList
		for(int i=0; i<food.size(); i++) {			//.size() returns the size(length) of an ArrayList
			System.out.println(food.get(i));		//.get(index) returns the value stored at the given index
		}
		
		//set Values
		food.set(0, "Sushi");						//replaces the String at index 0 with "Sushi"
		
		//remove Values
		food.remove(0);								//removes the stored value at given index
		
		//clear List
		food.clear(); 								//completely clears the ArrayList
		
		
		
		
		//2D ArrayLists = a dynamic list of lists
		
		ArrayList<ArrayList<String>> groceryList = new ArrayList<ArrayList<String>>();	//create an ArrayList that stores ArrayLists
		
		ArrayList<String> bakeryList = new ArrayList<String>();
		bakeryList.add("pasta");
		bakeryList.add("garlic bread");
		bakeryList.add("donuts");
		
		ArrayList<String> produceList = new ArrayList<String>();
		produceList.add("tomatoes");
		produceList.add("zucchini");
		produceList.add("peppers");
		
		ArrayList<String> drinksList = new ArrayList<String>();
		drinksList.add("soda");
		drinksList.add("coffee");
		
		groceryList.add(bakeryList);
		groceryList.add(produceList);
		groceryList.add(drinksList);
		
		System.out.println("\n2D ArrayList: ");
		System.out.println(groceryList);
		
		System.out.println("\nGet certain Lists of the ArrayList: ");
		System.out.println(groceryList.get(0));
		
		System.out.println("\nGet certain elements of one list within the ArrayList: ");
		System.out.println(groceryList.get(0).get(0));
		
		
	}
}
