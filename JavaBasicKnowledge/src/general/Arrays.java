package general;

public class Arrays {
	
	Arrays() {
		
		//array = used to store multiple values in a single variable
		String[] cars = {"Camaro","Corvette","Tesla","BMW"};
		// certain places in the array can be accessed via the usage of indices
		cars[0] = "Mustang"; // "Camaro" is replaces with "Mustang"
		System.out.println(cars[0]+"\n");
		
		// create an empty array with a certain amount of space
		String[] cars2 = new String[3]; //Array with 3 empty spaces (indices 0,1,2)
		cars2[0] = "Camaro";
		cars2[1] = "Corvette";
		cars2[2] = "Tesla";
		
		System.out.println("Array cars2: \n");
		// iterate through an array
		for(int i=0; i<cars2.length; i++) {
			System.out.println(cars2[i]);
		}
		
		
		// 2D - arrays = an array of arrays
		
		String[][] cars3 = new String[3][3]; //Array with 3 rows (first number) and 3 columns (second number)
		cars3[0][0] = "Camaro"; //item in row 0 and column 0 is set to "Camaro"
		cars3[0][1] = "Corvette"; //item in row 0 and column 1 is set to "Corvette"
		cars3[0][2] = "Silverado"; //...
		cars3[1][0] = "Mustang";
		cars3[1][1] = "Ranger";
		cars3[1][2] = "Tesla";
		cars3[2][0] = "Ferrari";
		cars3[2][1] = "Lambo";
		cars3[2][2] = "F-150";
		
		//display cars3 via a nested forLoop
		System.out.println("\n2D Array of cars: ");
		for(int i=0; i<cars3.length; i++) {		//cars.lenght returns the numb of rows
			System.out.println();
			for(int j=0; j<cars3[i].length; j++) { //cars3[i].length returns the numb of columns in the row i
				System.out.print(cars3[i][j]+" ");
			}
		}

		
	}
}
