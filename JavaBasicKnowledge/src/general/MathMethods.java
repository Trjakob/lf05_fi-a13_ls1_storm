package general;

public class MathMethods {
	
	MathMethods() {
		
		double x = 3.14;
		double y = -10;
		
		double max = Math.max(x, y);	//returns the larger of two numbs
		double min = Math.min(x, y);	//returns smaller of two numbs
		double abs = Math.abs(y);		//returns the absolute value of a numb (Betrag)
		double sqrt = Math.sqrt(x);		//returns the square root of a value
		double round = Math.round(x);	//rounds a certain value mathematically
		double ceil = Math.ceil(x);		//always rounds up a certain value
		double floor = Math.floor(x);	//always rounds down a certain value
		
		System.out.println("Math.max(3.14, -10)\t= "+max);
		System.out.println("Math.min(3.14, -10)\t= "+min);
		System.out.println("Math.abs(-10)\t\t= "+abs);
		System.out.println("Math.sqrt(3.14)\t\t= "+sqrt);
		System.out.println("Math.round(3.14)\t= "+round);
		System.out.println("Math.ceil(3.14)\t\t= "+ceil);
		System.out.println("Math.floor(3.14)\t= "+floor);
	}
}
