package general;

public class Loops {
	Loops() {
		
		// while - Loop -> executes a block of code while a certain condition evaluates as true
		// -> while Loops check the condition BEFORE running the code
		int x = 5;
		
		System.out.println("while Loop: ");
		while(x > 0) {
			System.out.println(x);
			x--;
		}
		
		System.out.println("END\n");
		
		
		// do Loop -> executes a block of code and while a certain condition is true it repeats executing
		// a do Loop executes its contained block of code at least once, even if the condition evaluates as false
		// -> do Loops check the condition AFTER running the code
		int y = 5;
		
		System.out.println("do-while Loop: ");
		do {
			System.out.println(y);
			y--;	
		} while(y>0);
		
		System.out.println("END\n");
		
		
		// for Loops -> executes a block of code a limited amount of times
		System.out.println("for Loop: ");
		for(int i=0; i<=5; i++) {
			System.out.println(i);
		}
		System.out.println("");
		
		
		// for-each loop = technique to iterate through all the elements in an array/collection(ex. ArrayList)
		String[] testArray = {"cat", "dog", "rat", "bird"};
		
		System.out.println("for-each Loop: ");
		for(String i : testArray) {		//for every element in testArray the block of code is executed
			System.out.println(i);
		}
		
		
	}
}
