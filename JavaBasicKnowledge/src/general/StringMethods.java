package general;

public class StringMethods {
	
	StringMethods() {
		
		// String = a reference data type that can store one or more characters
		//			reference data typed have access to useful methods
		
		String name = "Bruno";
		String name2 = "bruno";
		
		
		//.equals(String)
		boolean result = name.equals(name2); 	//compares 2 Strings
												//.equals is case sensitive (Capitalization matters!)
		System.out.println("\""+name+"\".equals(\""+name2+"\") -> "+result);
		
		
		//.equalsIgnoreCase(String)
		boolean result2 = name.equalsIgnoreCase(name2);	//compares 2 Strings
														//Capitalization does not matter!
		System.out.println("\""+name+"\".equalsIgnoreCase(\""+name2+"\") -> "+result2);
		
		
		//.length()
		int result3 = name.length();	//returns number of characters in a String
		
		System.out.println("\""+name+"\".length() -> "+result3);
		
		
		//.charAt(int)
		int index = 1;
		char result4 = name.charAt(index);	//returns the character at the given index in the String
		
		System.out.println("\""+name+"\".charAt("+index+") -> "+result4);
		
		
		//.indexOf(char)
		char testChar = 'n';
		int result5 = name.indexOf(testChar);	//finds a character in a String and returns its index within the String
		
		System.out.println("\""+name+"\".indexOf(\'"+testChar+"\') -> "+result5);
		
		
		//.isEmpty()
		boolean result6 = name.isEmpty();	//checks if a String is empty and returns a boolean value
		
		System.out.println("\""+name+"\".isEmtpy() -> "+result6);
		
		
		//.toUpperCase()
		String result7 = name.toUpperCase();	//changes all characters within a String to capitalized letters
		
		System.out.println("\""+name+"\".toUpperCase() -> \""+result7+"\"");
		
		
		//.toLowerCase()
		String result8 = name.toLowerCase();	//changes all characters within a String to non-capitalized letters
				
		System.out.println("\""+name+"\".toLowerCase() -> \""+result8+"\"");
		
		
		//.trim()
		String spaceName = "    Bruno   ";
		String result9 = spaceName.trim();		//removes all unnecessary empty spaces
		
		System.out.println("\""+spaceName+"\".trim() -> \""+result9+"\"");
		
		
		//.replace(oldChar, newChar)
		char oldChar = 'r';
		char newChar = 'x';
		String result10 = name.replace(oldChar, newChar);	//replaces a targeted character with a given new one
		
		System.out.println("\""+name+"\".replace(\'"+oldChar+"\',\'"+newChar+"\') -> \""+result10+"\"");
		
	}
}
