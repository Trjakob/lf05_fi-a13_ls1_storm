package general;

public class Expressions {
	
	// expression = operands + opperators
	// operands = values, variables, numbers, quantity
	// operators = + - * / %
	
	Expressions() {
		
		int friends = 10;
		System.out.println("friends = "+friends+"\n");
		
		//Addition +
		System.out.println("friends + 3 (addition)\t\t= "+(friends+3)); 
		
		//Subtraction -
		System.out.println("friends - 3 (subtraction)\t= "+(friends-3));
		
		//Multiplication *
		System.out.println("friends * 3 (multiplication)\t= "+(friends*3)); 
		
		//Division /
		System.out.println("friends / 3 (division)\t\t= "+(friends/3)); // (gibt eine ganze Zahl aus) (Division ohne Rest)
		
		//Modulo %
		System.out.println("friends % 3 (modulo)\t\t= "+(friends%3)); // (gibt der Rest der Ganzzahligen Division aus) (Rest-Funktion)
	}
}
