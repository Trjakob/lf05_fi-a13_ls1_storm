package general;

public class WrapperClasses {
	WrapperClasses() {
		
		//Wrapper class = 	provides a way to use primitive data types as reference data types
		//					reference data types contain useful methods but are slower to access
		
		//examples:
		//primitive		//wrapper
		//--------		//-------
		// boolean		Boolean
		// char			Character
		// int			Integer
		// double		Double
		// ...
		
		//autoboxing = 	the automatic conversion the Java compiler makes between the primitive type and its corresponding
		//				object wrapper class
		
		Boolean a = true;		//creates a reference type of Boolean (with the help of autoboxing)
		Character b = '@';		//creates a reference type of Character (with the help of autoboxing)
		Integer c = 123;		//creates a reference type of Integer (with the help of autoboxing)
		Double d = 3.14;		//creates a reference type of Double (with the help of autoboxing)
		String e = "Bruno";		//Strings are reference types by default
		
		//unboxing = the reverse of autoboxing. Automatic conversion of wrapper class to primitive
		//example:
		
		// reference data type used as primitive data type with the help of unboxing:
		if(a) {
			System.out.println("This is true");
		}
		
		// reference data type used as reference data type:
		if(a.booleanValue()) {
			System.out.println("This is also true");
		}
		
		
	}
}
