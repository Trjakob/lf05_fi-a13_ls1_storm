package MathematischeGrundalgorithmen;

import java.util.ArrayList;

// findet Primzahlen über einem gewissen Intervall

public class Sieb_des_Eratosthenes {
	
	//Listen als Klassenvariable anlegen
	ArrayList<Integer> numbList = new ArrayList<Integer>();
	ArrayList<Integer> scndNumbList = new ArrayList<Integer>();		//zweite wird im späteren Verlauf als Zwischenspeicher wichtig
	ArrayList<Integer> primes = new ArrayList<Integer>();			//Liste in denen die bereits gefundenen Primzahlen gespeichert werden
	int upperInt;		//obere Intervallgrenze
	
	
	public Sieb_des_Eratosthenes(int upperInt) {
		
		this.upperInt = upperInt;
		
		fill(this.upperInt,this.numbList);
		
		main();
		
		System.out.println("Primzahlen über dem Intervall i[1,"+this.upperInt+"] -> "+this.primes);
		
		
	}
	
	public void main() {
		while(!(this.numbList.isEmpty())) {
			sieve(this.upperInt, this.numbList);
			changeLists();
		}
	}
	
	public void fill(int upperInt, ArrayList<Integer> list) {
		//NummernListe mit allen ganzen Zahlen von 2 bis "upperInt" füllen
		for(int i=2; i<=upperInt; i++) {
			list.add(i);
		}
	}
	
	public void sieve(int upperInt, ArrayList<Integer> numbList) {
		//nimmt sich die erste Zahl der Liste und prüft jedes folgende Element auf Teilbarkeit
		
		int base = this.numbList.get(0);
		this.primes.add(base);	//die erste Zahl der Liste muss automatisch eine Primzahl sein, da sie noch nicht herausgefiltert wurde
		for(int j : numbList) {
			if(j%base != 0) {
				this.scndNumbList.add(j);
			}
		}
	}
	
	public void changeLists() {
		//wechselt die Listen numbList und scndNumbList und überträgt somit die Zahlen aus dem Zwischenspeicher
		this.numbList.clear();
		for(int j : this.scndNumbList) {
			this.numbList.add(j);
		}
		this.scndNumbList.clear();
	}
	
	
}
