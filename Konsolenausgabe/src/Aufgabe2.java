
public class Aufgabe2 {

	public static void main(String[] args) {
		
		//String s = Zeichen aus dem der Baum bestehen soll
		String s = "*";
		
		//Baum mit printf aufbauen / formatieren
		
		//Durch Erweiterung der maximalen Zeichenl�nge um 1 und paralleles hinzuf�gen von 2 Zeichen richtet sich der Baum mittig aus
		System.out.printf("\n%13s\n", s);
		System.out.printf("%14s\n", s+s+s);
		System.out.printf("%15s\n", s+s+s+s+s);
		System.out.printf("%16s\n", s+s+s+s+s+s+s);
		System.out.printf("%17s\n", s+s+s+s+s+s+s+s+s);
		System.out.printf("%18s\n", s+s+s+s+s+s+s+s+s+s+s);
		System.out.printf("%19s\n", s+s+s+s+s+s+s+s+s+s+s+s+s);
		System.out.printf("%14s\n", s+s+s);
		System.out.printf("%14s\n", s+s+s);

	}

}
