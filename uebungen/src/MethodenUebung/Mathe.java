package MethodenUebung;

public class Mathe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double k1= 10.0;
		double k2= 20.0;
		
		double hypotenuse = hypotenuse(k1,k2);
		System.out.print("das ergebnis ist = " + hypotenuse);
	}
	//Berechnung Quadrat
	public static double quadrat(double x) {
		return Math.pow(x, 2.0);
	}

	public static double hypotenuse(double kathete1, double kathete2) {
		
		double h = Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
		return h;
		
	}
	
	
	
	
}
